package com.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.dao.UserMapper;
import com.test.entity.User;
import com.test.service.IUserService;


@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserMapper userDao;
	
	
	@Override
	public User login(User user) {
		User u = userDao.selectByUserNameAndPassword(user);
		if(u != null) {
			return u;
		}
		return null;
	}

}
