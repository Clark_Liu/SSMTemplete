package com.test.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.test.entity.User;
import com.test.service.IUserService;

@Controller
public class UserController {

	@Autowired
	private IUserService userService;
	
	
	@RequestMapping("/login")
	public String login(User user,HttpSession session) {
		User u = userService.login(user);
		if(u != null) {
			session.setAttribute("user", u);
		}
		
		return "login.jsp";
	}
	
}
